<?php

namespace Drupal\mqclient_drush_env_based\Commands;

use Drupal\mqclient_drush\MqCommandsBase;

/**
 * A Drush commands class based on environment configuration.
 */
class MqEnvBasedCommands extends MqCommandsBase {

  /**
   * {@inheritdoc}
   */
  public function init(...$arguments) {
    if (empty($this->connectionSettings['MQ_SERVER_ADDRESS'])) {
      $this->connectionSettings = [
        'host' => getenv('MQ_SERVER_ADDRESS'),
        'port' => getenv('MQ_SERVER_PORT'),
        'channel' => getenv('MQ_SERVER_CHANNEL'),
        'queue_manager' => getenv('MQ_SERVER_QUEUE_MANAGER'),
        'key_repository' => getenv('MQ_SERVER_KEY_REPOSITORY'),
        'username' => getenv('MQ_SERVER_AUTH_USER'),
        'password' => getenv('MQ_SERVER_AUTH_PASS'),
      ];
    }
  }

}
