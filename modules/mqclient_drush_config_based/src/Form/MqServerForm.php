<?php

namespace Drupal\mqclient_drush_config_based\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the MqServer add and edit forms.
 */
class MqServerForm extends EntityForm {

  /**
   * The router builder service.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected RouteBuilderInterface $routeBuilder;

  /**
   * Instantiates this MqServerForm.
   *
   * @param \Drupal\Core\Routing\RouteBuilderInterface $router_builder
   *   The router builder service.
   */
  public function __construct(RouteBuilderInterface $router_builder) {
    $this->routeBuilder = $router_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('router.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\mqclient_drush_config_based\Entity\MqServer $entity */
    $entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t('A descriptive label of the server.'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => ['exists' => [$this, 'exists']],
      '#disabled' => !$entity->isNew(),
    ];
    $form['address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Server address'),
      '#default_value' => $entity->getAddress(),
      '#description' => $this->t('The address of the MQ server.'),
      '#required' => TRUE,
    ];
    $form['port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Server port'),
      '#default_value' => $entity->getPort(),
      '#description' => $this->t('The port of the MQ server.'),
      '#required' => TRUE,
    ];
    $form['channel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Server channel'),
      '#default_value' => $entity->getChannel(),
      '#description' => $this->t('The channel of the MQ server.'),
      '#required' => TRUE,
    ];
    $form['manager'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Server queue manager'),
      '#default_value' => $entity->getManager(),
      '#description' => $this->t('The queue manager of the MQ server.'),
      '#required' => TRUE,
    ];
    $form['key_repository'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Server key repository'),
      '#default_value' => $entity->getKeyRepository(),
      '#description' => $this->t('The key repository of the MQ server.'),
      '#required' => TRUE,
    ];
    $form['user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User'),
      '#default_value' => $entity->getUser(),
      '#description' => $this->t('Used for authentication.'),
      '#required' => TRUE,
    ];
    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#default_value' => $entity->getUser(),
      '#description' => $this->t('Used for authentication.'),
      '#required' => TRUE,
    ];
    $form['weight'] = [
      '#type' => 'number',
      '#title' => $this->t('Weight'),
      '#default_value' => $entity->get('weight'),
      '#description' => $this->t('Items will lower weight are processed first'),
      '#required' => TRUE,
    ];
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $entity->get('status'),
      '#description' => $this->t('Enabled servers will be processed in global processing'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = $this->entity->save();
    if ($result) {
      $this->messenger()->addMessage($this->t('Saved the %label MQ server.', ['%label' => $this->entity->label()]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label MQ server was not saved.', ['%label' => $this->entity->label()]), MessengerInterface::TYPE_ERROR);
    }

    $form_state->setRedirect('entity.mq_server.collection');

    return $result;
  }

  /**
   * Helper function to check whether an MQ server configuration entity exists.
   *
   * @param string $id
   *   The mq_server id.
   *
   * @return bool
   *   TRUE if the mq_server exists.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exists(string $id): bool {
    $entity = $this->entityTypeManager
      ->getStorage('mq_server')
      ->getQuery()
      ->accessCheck()
      ->condition('id', $id)
      ->execute();

    return (bool) $entity;
  }

}
