<?php

namespace Drupal\mqclient_drush_config_based\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\mqclient_drush_config_based\Entity\MqServer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the MqQueue add and edit forms.
 */
class MqQueueForm extends EntityForm {

  /**
   * The router builder service.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected RouteBuilderInterface $routeBuilder;

  /**
   * Instantiates this MqQueueForm.
   *
   * @param \Drupal\Core\Routing\RouteBuilderInterface $router_builder
   *   The router builder service.
   */
  public function __construct(RouteBuilderInterface $router_builder) {
    $this->routeBuilder = $router_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('router.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\mqclient_drush_config_based\Entity\MqQueue $entity */
    $entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t('A descriptive label of the queue.'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => ['exists' => [$this, 'exists']],
      '#disabled' => !$entity->isNew(),
    ];
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Queue name'),
      '#default_value' => $entity->getName(),
      '#description' => $this->t('The name of the MQ queue.'),
      '#required' => TRUE,
    ];
    $form['resource'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Associated resource class'),
      '#default_value' => $entity->getResource(),
      '#description' => $this->t('The resource class to be instantiated.'),
      '#required' => TRUE,
    ];
    $form['server'] = [
      '#type' => 'select',
      '#title' => $this->t('MQ Server'),
      '#description' => $this->t('The MQ server for this queue.'),
      '#options' => $this->serverList(),
      '#default_value' => $entity->getServer(),
      '#required' => TRUE,
    ];
    $form['weight'] = [
      '#type' => 'number',
      '#title' => $this->t('Weight'),
      '#default_value' => $entity->get('weight'),
      '#description' => $this->t('Items will lower weight are processed first'),
      '#required' => TRUE,
    ];
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $entity->get('status'),
      '#description' => $this->t('Enabled queues will be processed in global processing'),
    ];
    return $form;
  }

  /**
   * Helper function to get the list of enabled MQ servers.
   *
   * @return array
   *   An array of options.
   */
  protected function serverList(): array {
    $servers = MqServer::loadMultiple();
    $options = [];
    foreach ($servers as $server) {
      $options[$server->id()] = $server->label();
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = $this->entity->save();
    if ($result) {
      $this->messenger()->addMessage($this->t('Saved the %label MQ queue.', ['%label' => $this->entity->label()]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label MQ queue was not saved.', ['%label' => $this->entity->label()]), MessengerInterface::TYPE_ERROR);
    }

    $form_state->setRedirect('entity.mq_queue.collection');

    return $result;
  }

  /**
   * Helper function to check whether an MQ queue configuration entity exists.
   *
   * @param string $id
   *   The mq_queue id.
   *
   * @return bool
   *   TRUE if the mq_queue exists.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exists(string $id): bool {
    $entity = $this->entityTypeManager
      ->getStorage('mq_queue')
      ->getQuery()
      ->accessCheck()
      ->condition('id', $id)
      ->execute();

    return (bool) $entity;
  }

}
