<?php

namespace Drupal\mqclient_drush_config_based\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the MqQueue entity.
 *
 * @ConfigEntityType(
 *   id = "mq_queue",
 *   label = @Translation("MQ queue"),
 *   label_collection = @Translation("MQ"),
 *   label_plural = @Translation("MQ queues"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\mqclient_drush_config_based\Controller\MqQueueListBuilder",
 *     "form" = {
 *       "add" = "Drupal\mqclient_drush_config_based\Form\MqQueueForm",
 *       "edit" = "Drupal\mqclient_drush_config_based\Form\MqQueueForm",
 *       "delete" = "Drupal\mqclient_drush_config_based\Form\MqQueueDeleteForm"
 *     },
 *   },
 *   config_prefix = "mq_queue",
 *   admin_permission = "administer mq",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "weight" = "weight"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "status",
 *     "name",
 *     "resource",
 *     "server",
 *     "weight"
 *   },
 *   links = {
 *     "canonical" = "/mq/queue/{mq_queue}",
 *     "add-form" = "/admin/config/mq/queues/add",
 *     "edit-form" = "/admin/config/mq/queues/{mq_queue}/edit",
 *     "delete-form" = "/admin/config/mq/queues/{mq_queue}/delete",
 *     "collection" = "/admin/config/mq/queues",
 *     "enable" = "/admin/config/mq/queues/{mq_queue}/enable",
 *     "disable" = "/admin/config/mq/queues/{mq_queue}/disable",
 *   }
 * )
 */
class MqQueue extends ConfigEntityBase implements MqQueueInterface {

  /**
   * The searcher ID.
   *
   * @var string
   */
  public $id;

  /**
   * The searcher label.
   *
   * @var string
   */
  public $label;

  /**
   * Name of the MQ queue.
   *
   * @var string
   */
  protected $name;

  /**
   * Fully qualified resource class name.
   *
   * @var string
   */
  protected $resource;

  /**
   * Fully qualified resource class name.
   *
   * @var string
   */
  protected $server;

  /**
   * Queue weight.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function getResource() {
    return $this->resource;
  }

  /**
   * {@inheritdoc}
   */
  public function getServer() {
    return $this->server;
  }

}
