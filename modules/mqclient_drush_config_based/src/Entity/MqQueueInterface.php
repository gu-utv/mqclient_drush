<?php

namespace Drupal\mqclient_drush_config_based\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Configuration Layer setting entities.
 */
interface MqQueueInterface extends ConfigEntityInterface {

  /**
   * {@inheritdoc}
   */
  public function getName();

  /**
   * {@inheritdoc}
   */
  public function getResource();

  /**
   * {@inheritdoc}
   */
  public function getServer();

}
