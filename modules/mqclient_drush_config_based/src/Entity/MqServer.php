<?php

namespace Drupal\mqclient_drush_config_based\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use gu\mqclient\MqConnection;

/**
 * Defines the MqServer entity.
 *
 * @ConfigEntityType(
 *   id = "mq_server",
 *   label = @Translation("MQ server"),
 *   label_collection = @Translation("MQ"),
 *   label_plural = @Translation("MQ servers"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\mqclient_drush_config_based\Controller\MqServerListBuilder",
 *     "form" = {
 *       "add" = "Drupal\mqclient_drush_config_based\Form\MqServerForm",
 *       "edit" = "Drupal\mqclient_drush_config_based\Form\MqServerForm",
 *       "delete" = "Drupal\mqclient_drush_config_based\Form\MqServerDeleteForm"
 *     },
 *   },
 *   config_prefix = "mq_server",
 *   admin_permission = "administer mq",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "weight" = "weight",
 *     "status" = "status"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "address",
 *     "port",
 *     "channel",
 *     "manager",
 *     "key_repository",
 *     "user",
 *     "password",
 *     "weight",
 *     "status"
 *   },
 *   links = {
 *     "canonical" = "/mq/server/{mq_server}",
 *     "add-form" = "/admin/config/mq/servers/add",
 *     "edit-form" = "/admin/config/mq/servers/{mq_server}/edit",
 *     "delete-form" = "/admin/config/mq/servers/{mq_server}/delete",
 *     "collection" = "/admin/config/mq/servers"
 *   }
 * )
 */
class MqServer extends ConfigEntityBase {

  /**
   * The searcher ID.
   *
   * @var string
   */
  public $id;

  /**
   * The server label.
   *
   * @var string
   */
  public $label;

  /**
   * MQ server address.
   *
   * @var string
   */
  protected $address;

  /**
   * MQ server port.
   *
   * @var string
   */
  protected $port;

  /**
   * MQ server channel.
   *
   * @var string
   */
  protected $channel;

  /**
   * MQ server queue manager.
   *
   * @var string
   */
  protected $manager;

  /**
   * MQ server key repository.
   *
   * @var string
   */
  protected $key_repository;

  /**
   * MQ server user.
   *
   * @var string
   */
  protected $user;

  /**
   * MQ server password.
   *
   * @var string
   */
  protected $password;

  /**
   * Server weight.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * Server status.
   *
   * @var int
   */
  protected $status = 0;

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $resolver;

  /**
   * The account switcher service.
   *
   * @var \Drupal\Core\Session\AccountSwitcherInterface
   */
  protected $accountSwitcher;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The MQ connection.
   *
   * @var \gu\mqclient\MqConnection
   */
  protected MqConnection $mq;

  /**
   * The logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function getAddress() {
    return $this->address;
  }

  /**
   * {@inheritdoc}
   */
  public function getPort() {
    return $this->port;
  }

  /**
   * {@inheritdoc}
   */
  public function getChannel() {
    return $this->channel;
  }

  /**
   * {@inheritdoc}
   */
  public function getManager() {
    return $this->manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getKeyRepository() {
    return $this->key_repository;
  }

  /**
   * {@inheritdoc}
   */
  public function getUser() {
    return $this->user;
  }

  /**
   * {@inheritdoc}
   */
  public function getPassword() {
    return $this->password;
  }

}
