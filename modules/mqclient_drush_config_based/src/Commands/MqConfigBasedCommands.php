<?php

namespace Drupal\mqclient_drush_config_based\Commands;

use Drupal\mqclient_drush\MqCommandsBase;
use Drupal\mqclient_drush_config_based\Entity\MqQueue;
use Drupal\mqclient_drush_config_based\Entity\MqQueueInterface;
use Drupal\mqclient_drush_config_based\Entity\MqServer;
use gu\mqclient\envelopes\MqQueueMessageEnvelopeInterface;

/**
 * Mq Drush commands based on configuration objects.
 */
class MqConfigBasedCommands extends MqCommandsBase {

  /**
   * {@inheritdoc}
   */
  public function init(...$arguments) {
    if (empty($arguments)) {
      return NULL;
    }
    if (empty($arguments[0])) {
      return NULL;
    }
    $id = $arguments[0];
    $server = $this->entityTypeManager->getStorage('mq_server')->load($id);
    if (!$server instanceof MqServer) {
      // First argument is not a server id, let's look for a queue.
      $queue = $this->entityTypeManager->getStorage('mq_queue')->load($id);
      if (!$queue instanceof MqQueue) {
        return NULL;
      }
      $server = $this->entityTypeManager->getStorage('mq_server')->load($queue->getServer());
    }

    if (!$server instanceof MqServer) {
      return NULL;
    }
    $this->connectionSettings = [
      'host' => $server->getAddress(),
      'port' => $server->getPort(),
      'channel' => $server->getChannel(),
      'queue_manager' => $server->getManager(),
      'key_repository' => $server->getKeyRepository(),
      'username' => $server->getUser(),
      'password' => $server->getPassword(),
    ];
    if (isset($queue) && $queue instanceof MqQueueInterface) {
      return $queue;
    }
    return $server;
  }

  /**
   * Process all enabled queue objects.
   *
   * @param array $options
   *   The command options.
   *
   * @command mq:config_based:process
   * @aliases mq:consume
   *
   * @option max_message_size
   *   The maximum message size to process, as integer, in bytes.
   *   Default: 4194304 (4 MB).
   * @options persist
   *   Persist the incoming message in Drupal.
   *   Default: false.
   * @options commit
   *   Commit read messages to MQ.
   *   Default: false.
   * @option count
   *   How many messages to read.
   *   Default: 10.
   * @option user
   *   A user id. The command will run as this user.
   *   Default: 0.
   *
   * @return int
   *   A return value to be used in error monitoring.
   */
  public function processAll(array $options = [
    'max_message_size' => MqQueueMessageEnvelopeInterface::MAX_MESSAGE_SIZE_DEFAULT,
    'persist' => FALSE,
    'commit' => FALSE,
    'count' => 10,
    'user' => 0,
  ]) {
    $server_ids = $this->entityTypeManager
      ->getStorage('mq_server')
      ->getQuery()
      ->accessCheck()
      ->condition('status', 1)
      ->sort('weight', 'ASC')
      ->execute();
    if (empty($server_ids)) {
      return NULL;
    }
    foreach ($server_ids as $server_id) {
      $queue_ids = $this->entityTypeManager
        ->getStorage('mq_queue')
        ->getQuery()
        ->accessCheck()
        ->condition('status', 1)
        ->condition('server', $server_id)
        ->sort('weight', 'ASC')
        ->execute();
      if (empty($queue_ids)) {
        return NULL;
      }
      $this->init($server_id);
      $this->connect();
      $queues = $this->entityTypeManager->getStorage('mq_queue')->loadMultiple($queue_ids);
      foreach ($queues as $queue) {
        /** @var \Drupal\mqclient_drush_config_based\Entity\MqQueue $queue */
        $this->processQueue($options['user'], $queue->getName(), $queue->getResource(), (bool) $options['persist'], (bool) $options['commit'], (int) $options['count'], (int) $options['max_message_size']);
      }
    }
    return 0;

  }

  /**
   * Process a queue config object.
   *
   * @param string $queue_id
   *   The id of the queue config entity.
   * @param array $options
   *   The command options.
   *
   * @command mq:config_based:process_queue
   * @aliases mq_process_queue
   *
   * @option max_message_size
   *   The maximum message size to process, as integer, in bytes.
   *   Default: 4194304 (4 MB).
   * @options persist
   *   Persist the incoming message in Drupal.
   *   Default: false.
   * @options commit
   *   Commit read messages to MQ.
   *   Default: false.
   * @option count
   *   How many messages to read.
   *   Default: 10.
   * @option user
   *   A user id. The command will run as this user.
   *   Default: 0.
   *
   * @return int
   *   A return value to be used in error monitoring.
   */
  public function process(string $queue_id, array $options = [
    'max_message_size' => MqQueueMessageEnvelopeInterface::MAX_MESSAGE_SIZE_DEFAULT,
    'persist' => FALSE,
    'commit' => FALSE,
    'count' => 10,
    'user' => 0,
  ]): int {
    $queue = $this->init($queue_id);
    if (!($queue instanceof MqQueueInterface)) {
      $this->logger->error('Queue config entity "{@queue_id}" is not defined', ['@queue_id' => $queue_id]);
      return MQSERIES_MQCC_FAILED;
    }

    $this->connect();

    return $this->processQueue($options['user'], $queue->getName(), $queue->getResource(), (bool) $options['persist'], (bool) $options['commit'], (int) $options['count'], (int) $options['max_message_size']);
  }

  /**
   * Command to test a connection.
   *
   * @param string $server_id
   *   The id of a server config entity.
   *
   * @command mq:config_based:test_server
   */
  public function testConnection(string $server_id) {
    $server = $this->init($server_id);
    if ($server instanceof MqServer) {
      return $this->connectionTest();
    }
    else {
      $this->logger->error('Server config entity "{@server_id}" is not defined', ['@server_id' => $server_id]);
    }
  }

  /**
   * Command to test a reading a message from a queue.
   *
   * @param string $queue_id
   *   The id of a server config entity.
   *
   * @command mq:config_based:test_queue
   */
  public function testQueue(string $queue_id) {
    $queue = $this->init($queue_id);
    if ($queue instanceof MqQueueInterface) {
      $message = (string) $this->queueTest($queue->getName());
      $this->logger->info('Testing queue config entity "{@queue_id}" generated this message: {@message}', [
        '@queue_id' => $queue_id,
        '@message' => $message,
      ]);
    }
    else {
      $this->logger->error('Queue config entity "{@queue_id}" is not defined', ['@queue_id' => $queue_id]);
    }
  }

}
