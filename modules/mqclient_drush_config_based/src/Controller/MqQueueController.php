<?php

namespace Drupal\mqclient_drush_config_based\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\mqclient_drush_config_based\Entity\MqQueueInterface;

/**
 * Route controller for MQ queues.
 */
class MqQueueController extends ControllerBase {

  /**
   * Performs an operation on the config layer entity.
   *
   * @param \Drupal\mqclient_drush_config_based\Entity\MqQueueInterface $mq_queue
   *   The config layer entity.
   * @param string $op
   *   The operation to perform, usually 'enable' or 'disable'.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect back to the config layers settings page.
   */
  public function performOperation(MqQueueInterface $mq_queue, $op) {
    $mq_queue->$op()->save();

    $actions = [
      'enable' => 'enabled',
      'disable' => 'disabled',
    ];
    \Drupal::messenger()->addStatus($this->t('The %label queue has been %action.',
      ['%label' => $mq_queue->label(), '%action' => $actions[$op]])
    );

    $url = $mq_queue->toUrl('collection');
    return $this->redirect($url->getRouteName(), $url->getRouteParameters(), $url->getOptions());
  }

}
