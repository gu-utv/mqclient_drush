<?php

namespace Drupal\mqclient_drush_config_based\Controller;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of MQ servers.
 */
class MqServerListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mqclient_config_mqserver_list';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['label'] = $this->t('Label');
    $header['host'] = $this->t('Address');
    $header['port'] = $this->t('Port');
    $header['channel'] = $this->t('Channel');
    $header['manager'] = $this->t('Manager');
    $header['user'] = $this->t('User');
    $header['status'] = $this->t('Enabled');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    /** @var \Drupal\mqclient_drush_config_based\Entity\MqServer $entity */
    $row['label'] = $entity->label();
    $row['host'] = [
      "#type" => "markup",
      "#markup" => $entity->getAddress(),
    ];
    $row['port'] = [
      "#type" => "markup",
      "#markup" => $entity->getPort(),
    ];
    $row['channel'] = [
      "#type" => "markup",
      "#markup" => $entity->getChannel(),
    ];
    $row['manager'] = [
      "#type" => "markup",
      "#markup" => $entity->getManager(),
    ];
    $row['user'] = [
      "#type" => "markup",
      "#markup" => $entity->getUser(),
    ];
    $row['status'] = [
      '#type' => 'markup',
      '#markup' => $entity->get('status') ? $this->t('Yes') : $this->t('No'),
    ];
    return $row + parent::buildRow($entity);
  }

}
