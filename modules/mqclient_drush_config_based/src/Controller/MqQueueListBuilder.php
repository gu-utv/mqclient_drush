<?php

namespace Drupal\mqclient_drush_config_based\Controller;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of MQ queues.
 */
class MqQueueListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mqclient_config_queues_list';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['name'] = $this->t('Queue name');
    $header['resource'] = $this->t('Resource class');
    $header['server'] = $this->t('Server name');
    $header['status'] = $this->t('Enabled');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\mqclient_drush_config_based\Entity\MqQueue $entity */
    $row['label'] = $entity->label();
    $row['name'] = [
      "#type" => "markup",
      "#markup" => $entity->getName(),
    ];
    $row['resource'] = [
      "#type" => "markup",
      "#markup" => $entity->getResource(),
    ];
    $row['server'] = [
      "#type" => "markup",
      "#markup" => $entity->getServer(),
    ];
    $row['status'] = [
      "#type" => "markup",
      "#markup" => $entity->get('status') ? $this->t('Yes') : $this->t('No'),
    ];
    return $row + parent::buildRow($entity);
  }

}
