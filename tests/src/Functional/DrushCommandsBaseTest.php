<?php

namespace Drupal\Tests\mqclient_drush\Functional;

use Drupal\Core\Session\AccountInterface;
use Drupal\mqclient_drush\MqCommandsBase;
use Drupal\Tests\BrowserTestBase;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Functional tests for the base command.
 */
class DrushCommandsBaseTest extends BrowserTestBase {

  use ProphecyTrait;
  /**
   * {@inheritdoc}
   */
  protected static $modules = ['mqclient_drush'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * Get a methos (usually protected) from a class.
   *
   * @param string $name
   *   The method name.
   *
   * @return \ReflectionMethod
   *   An invokable method.
   */
  protected static function getMethod($name): \ReflectionMethod {
    $class = new \ReflectionClass(MqCommandsBase::class);
    $method = $class->getMethod($name);
    $method->setAccessible(TRUE);
    return $method;
  }

  /**
   * Test for the getAccount method.
   */
  public function testGetAccount() {
    $authorized = $this->drupalCreateUser(['connect to mq']);
    $unauthorized = $this->drupalCreateUser();
    $base = $this->prophesize(MqCommandsBase::class);
    $base->willBeConstructedWith([
      $this->container->get('mqclient_drush.manager'),
      $this->container->get('account_switcher'),
      $this->container->get('entity_type.manager'),
    ]);
    $base = $base->reveal();
    $method = self::getMethod('getAccount');

    $this->assertInstanceOf(AccountInterface::class, $method->invoke($base, $authorized->id()));
    $this->assertInstanceOf(AccountInterface::class, $method->invoke($base, $authorized->getAccountName()));

    $this->assertNull($method->invoke($base, $unauthorized->id()));
    $this->assertNull($method->invoke($base, $unauthorized->getAccountName()));
  }

}
