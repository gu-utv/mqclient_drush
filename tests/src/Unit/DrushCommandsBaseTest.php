<?php

namespace Drupal\Tests\mqclient_drush\Unit;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\mqclient_drush\MqCommandsBase;
use Drupal\mqclient_drush\MqManager;
use Drupal\Tests\UnitTestCase;
use Drush\Log\DrushLoggerManager;
use gu\mqclient\envelopes\MqodEnvelopeInterface;
use gu\mqclient\MqConnection;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Drush command tests.
 *
 * @coversDefaultClass \Drupal\mqclient_drush\MqCommandsBase
 */
class DrushCommandsBaseTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * Get a methos (usually protected) from a class.
   *
   * @param string $name
   *   The method name.
   *
   * @return \ReflectionMethod
   *   An invokable method.
   */
  protected static function getMethod(string $name): \ReflectionMethod {
    $class = new \ReflectionClass(MqCommandsBase::class);
    $method = $class->getMethod($name);
    $method->setAccessible(TRUE);
    return $method;
  }

  /**
   * Test whether we can connect successfully.
   *
   * @param bool $connection_status
   *   Whether the connection test is successful.
   *
   * @covers ::__construct
   * @covers ::__destruct
   * @covers ::connect
   * @dataProvider connectProvider
   */
  public function testConnect(bool $connection_status): void {
    $connection = $this->prophesize(MqConnection::class);
    $manager = $this->prophesize(MqManager::class);
    $account_switcher = $this->prophesize(AccountSwitcherInterface::class);
    $entity_type_manager = $this->prophesize(EntityTypeManagerInterface::class);
    $base = $this->prophesize(MqCommandsBase::class);
    if ($connection_status) {
      $manager->getConnection(Argument::any(), Argument::any(), Argument::any(), Argument::any(), Argument::any(), Argument::any(), Argument::any())->WillReturn($connection->reveal());
      $base->willBeConstructedWith([
        $manager->reveal(),
        $account_switcher->reveal(),
        $entity_type_manager->reveal(),
      ]);
    }
    else {
      $manager->getConnection(Argument::any(), Argument::any(), Argument::any(), Argument::any(), Argument::any(), Argument::any(), Argument::any())->WillThrow(\Exception::class);
      $base->willBeConstructedWith([
        $manager->reveal(),
        $account_switcher->reveal(),
        $entity_type_manager->reveal(),
      ]);
      $this->expectException(\Exception::class);
    }
    $connect_method = self::getMethod('connect');
    $get_connection_method = self::getMethod('getConnection');
    $connect_method->invoke($base->reveal());
    if ($connection_status) {
      $this->assertNotNull($get_connection_method->invoke($base->reveal()));
    }
    else {
      $this->assertNull($get_connection_method->invoke($base->reveal()));
    }
  }

  /**
   * Provider for the connect test.
   *
   * @return array
   *   The provided test cases.
   */
  public function connectProvider() {
    return [
      [TRUE],
      [FALSE],
    ];
  }

  /**
   * Test whether we can disconnect successfully.
   *
   * @param bool $connected
   *   Whether the connection connected.
   *
   * @covers ::__construct
   * @covers ::__destruct
   * @covers ::disconnect
   * @dataProvider disconnectProvider
   */
  public function testDisconnect(bool $connected) {
    $connection = $this->prophesize(MqConnection::class);
    $manager = $this->prophesize(MqManager::class);
    $account_switcher = $this->prophesize(AccountSwitcherInterface::class);
    $entity_type_manager = $this->prophesize(EntityTypeManagerInterface::class);
    $base = $this->prophesize(MqCommandsBase::class);
    $base->willBeConstructedWith([
      $manager->reveal(),
      $account_switcher->reveal(),
      $entity_type_manager->reveal(),
    ]);
    $logger = $this->prophesize(DrushLoggerManager::class);
    if ($connected) {
      $logger->debug(Argument::any(), Argument::any())->shouldBeCalled();
    }
    else {
      $logger->debug(Argument::any(), Argument::any())->shouldNotBeCalled();
    }
    $base = $base->reveal();
    $set_logger_method = self::getMethod('setLogger');
    $set_logger_method->invoke($base, $logger->reveal());
    if ($connected) {
      $connection->disconnect()->shouldBeCalled();
      $set_connection_method = self::getMethod('setConnection');
      $set_connection_method->invoke($base, $connection->reveal());
    }
    $disconnect_method = self::getMethod('disconnect');
    $disconnect_method->invoke($base);

  }

  /**
   * Provider for the disconnect test.
   *
   * @return array
   *   The provided test cases.
   */
  public function disconnectProvider() {
    return [
      [TRUE],
      [FALSE],
    ];
  }

  /**
   * Test whether we can get a queue successfully.
   *
   * @param bool $connected
   *   Whether the connection connected.
   * @param bool $successful
   *   Whether to expect a successful operation.
   *
   * @covers ::__construct
   * @covers ::__destruct
   * @covers ::openQueue
   * @dataProvider openQueueProvider
   */
  public function testOpenQueue(bool $connected, bool $successful) {
    $connection = $this->prophesize(MqConnection::class);
    $manager = $this->prophesize(MqManager::class);
    $account_switcher = $this->prophesize(AccountSwitcherInterface::class);
    $entity_type_manager = $this->prophesize(EntityTypeManagerInterface::class);
    $base = $this->prophesize(MqCommandsBase::class);
    $queue = $this->prophesize(MqodEnvelopeInterface::class);
    if ($successful) {
      $manager->getQueue(Argument::any(), Argument::any(), Argument::any())->willReturn($queue->reveal());
    }
    else {
      $manager->getQueue(Argument::any(), Argument::any(), Argument::any())->willThrow(\Exception::class);
      if ($connected) {
        $this->expectException(\exception::class);
      }
      else {
        $this->expectException(\TypeError::class);
      }
    }
    $base->willBeConstructedWith([
      $manager->reveal(),
      $account_switcher->reveal(),
      $entity_type_manager->reveal(),
    ]);
    $base = $base->reveal();
    if ($connected) {
      $set_connection_method = self::getMethod('setConnection');
      $set_connection_method->invoke($base, $connection->reveal());
    }
    $open_queue_method = self::getMethod('openQueue');
    $result = $open_queue_method->invoke($base, 'something');
    if ($successful) {
      $this->assertNotNull($result);
    }

  }

  /**
   * Provider for the open queue test.
   *
   * @return array
   *   The provided test cases.
   */
  public function openQueueProvider() {
    return [
      [TRUE, TRUE],
      [TRUE, FALSE],
      [FALSE, FALSE],
      // False TRUE is impossible.
    ];
  }

  /**
   * Test whether we can read from a queue successfully.
   *
   * @param bool $connected
   *   Whether the connection connected.
   * @param bool $successful
   *   Whether to expect a successful operation.
   *
   * @covers ::__construct
   * @covers ::__destruct
   * @covers ::readFromQueue
   * @dataProvider readFromQueueProvider
   */
  public function testReadFromQueue(bool $connected, bool $successful) {
    $connection = $this->prophesize(MqConnection::class);
    $manager = $this->prophesize(MqManager::class);
    $account_switcher = $this->prophesize(AccountSwitcherInterface::class);
    $entity_type_manager = $this->prophesize(EntityTypeManagerInterface::class);
    $base = $this->prophesize(MqCommandsBase::class);
    $queue = $this->prophesize(MqodEnvelopeInterface::class);
    if ($successful) {
      $manager->readFromQueue(Argument::any(), Argument::any(), Argument::any())->willReturn('message');
    }
    else {
      $manager->readFromQueue(Argument::any(), Argument::any(), Argument::any())->willThrow(\Exception::class);
      if ($connected) {
        $this->expectException(\exception::class);
      }
      else {
        $this->expectException(\TypeError::class);
      }
    }
    $base->willBeConstructedWith([
      $manager->reveal(),
      $account_switcher->reveal(),
      $entity_type_manager->reveal(),
    ]);
    $base = $base->reveal();
    if ($connected) {
      $set_connection_method = self::getMethod('setConnection');
      $set_connection_method->invoke($base, $connection->reveal());
    }
    $read_from_queue_method = self::getMethod('readFromQueue');
    $result = $read_from_queue_method->invoke($base, $queue->reveal());
    if ($successful) {
      $this->assertNotNull($result);
    }

  }

  /**
   * Provider for the read from queue test.
   *
   * @return array
   *   The provided test cases.
   */
  public function readFromQueueProvider() {
    return [
      [TRUE, TRUE],
      [TRUE, FALSE],
      [FALSE, FALSE],
      // False True is impossible.
    ];
  }

}
