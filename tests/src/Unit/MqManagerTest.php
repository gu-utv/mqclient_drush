<?php

namespace Drupal\Tests\mqclient_drush\Unit;

use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\mqclient_drush\MqDrushResourceInterface;
use Drupal\mqclient_drush\MqManager;
use Drupal\mqclient_drush\Resource\StdoutResource;
use Drupal\Tests\UnitTestCase;
use gu\mqclient\envelopes\EnvelopeFactory;
use gu\mqclient\envelopes\EnvelopeFactoryInterface;
use gu\mqclient\envelopes\EnvelopeInterface;
use gu\mqclient\envelopes\MqcnoEnvelopeInterface;
use gu\mqclient\envelopes\MqEnvelope;
use gu\mqclient\envelopes\MqgmoEnvelopeInterface;
use gu\mqclient\envelopes\MqodEnvelopeInterface;
use gu\mqclient\MqConnection;
use gu\mqclient\stamps\MqcnoStamp;
use gu\mqclient\stamps\MqodStamp;
use gu\mqclient\stamps\StampFactory;
use gu\mqclient\stamps\StampFactoryInterface;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * MQ Manager tests.
 *
 * @coversDefaultClass \Drupal\mqclient_drush\MaManager
 */
class MqManagerTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * Test whether we can connect successfully.
   *
   * @param bool $connection_status
   *   Whether the connection test is successful.
   * @param bool $ssl
   *   Wether to enable SSL.
   * @param bool $auth
   *   Whether to enable user/password authentication.
   *
   * @covers ::__construct
   * @covers ::__destruct
   * @covers ::getConnection
   * @dataProvider getConnectionProvider
   */
  public function testGetConnection(bool $connection_status, bool $ssl, bool $auth) {
    $mqcno_stamp = $this->prophesize(MqcnoStamp::class);
    $mqcno_envelope = $this->prophesize(MqcnoEnvelopeInterface::class);
    $connection = $this->prophesize(MqConnection::class);
    $stamp_factory = $this->prophesize(StampFactoryInterface::class);
    $envelope_factory = $this->prophesize(EnvelopeFactoryInterface::class);
    $resolver = $this->prophesize(ClassResolverInterface::class);
    $logger_factory = $this->prophesize(LoggerChannelFactoryInterface::class);
    $logger = $this->prophesize(LoggerChannelInterface::class);

    if ($ssl) {
      $mqcno_stamp->enableSsl(Argument::any())->shouldBeCalled();
      $repo = 'somepath';
    }
    else {
      $mqcno_stamp->enableSsl(Argument::any())->shouldNotBeCalled();
      $repo = '';
    }

    if ($auth) {
      $mqcno_stamp->setAuth(Argument::any(), Argument::any())->shouldBeCalled();
      $user = 'somepath';
    }
    else {
      $mqcno_stamp->setAuth(Argument::any(), Argument::any())->shouldNotBeCalled();
      $user = '';
    }
    $stamp_factory->createConnectionStamp(Argument::any(), Argument::any(), Argument::any())->willReturn($mqcno_stamp->reveal());

    $envelope_factory->createConnectionEnvelope(Argument::any(), Argument::any())->willReturn($mqcno_envelope->reveal());

    $resolver->getInstanceFromDefinition(StampFactory::class)->WillReturn($stamp_factory);
    $resolver->getInstanceFromDefinition(EnvelopeFactory::class)->willReturn($envelope_factory);

    if ($connection_status) {
      $connection->connect(Argument::any())->shouldBeCalled();
    }
    else {
      $connection->connect(Argument::any())->willThrow(\Exception::class);
      $this->expectException(\exception::class);
    }

    $resolver->getInstanceFromDefinition(MqConnection::class)->willReturn($connection->reveal());
    $logger_factory->get(Argument::any())->willReturn($logger->reveal());

    $manager = new MqManager($resolver->reveal(), $logger_factory->reveal());

    $result = $manager->getConnection('', 0, '', '', $repo, $user, '');
    if ($connection_status) {
      $this->assertInstanceOf(MqConnection::class, $result);
    }
  }

  /**
   * Provider for the getConnection test.
   *
   * @return array
   *   The provided test cases.
   */
  public function getConnectionProvider() {
    return [
      [TRUE, TRUE, TRUE],
      [TRUE, TRUE, FALSE],
      [TRUE, FALSE, TRUE],
      [TRUE, FALSE, FALSE],
      [FALSE, TRUE, TRUE],
      [FALSE, TRUE, FALSE],
      [FALSE, FALSE, TRUE],
      [FALSE, FALSE, FALSE],
    ];
  }

  /**
   * Test whether we can connect successfully.
   *
   * @param bool $get_status
   *   Whether the queue open openration is successful.
   *
   * @covers ::__construct
   * @covers ::__destruct
   * @covers ::getQueue
   * @dataProvider getQueueProvider
   */
  public function testGetQueue($get_status) {
    $connection = $this->prophesize(MqConnection::class);
    $stamp_factory = $this->prophesize(StampFactoryInterface::class);
    $envelope_factory = $this->prophesize(EnvelopeFactoryInterface::class);
    $resolver = $this->prophesize(ClassResolverInterface::class);
    $logger_factory = $this->prophesize(LoggerChannelFactoryInterface::class);
    $logger = $this->prophesize(LoggerChannelInterface::class);
    $stamp = $this->prophesize(MqodStamp::class);
    $envelope = $this->prophesize(MqodEnvelopeInterface::class);

    $stamp_factory->createOpenObjectStamp(Argument::any())->willReturn($stamp->reveal());
    $resolver->getInstanceFromDefinition(StampFactory::class)->willReturn($stamp_factory->reveal());

    $envelope_factory->createOpenObjectEnvelope(Argument::any(), Argument::any())->willReturn($envelope->reveal());
    $resolver->getInstanceFromDefinition(EnvelopeFactory::class)->willReturn($envelope_factory->reveal());

    if ($get_status) {
      $connection->open(Argument::any())->shouldBeCalled();
    }
    else {
      $connection->open(Argument::any())->willThrow(\Exception::class);
      $this->expectException(\exception::class);
    }
    $logger_factory->get(Argument::any())->willReturn($logger->reveal());
    $manager = new MqManager($resolver->reveal(), $logger_factory->reveal());
    $result = $manager->getQueue($connection->reveal(), '', TRUE);
    if ($get_status) {
      $this->assertInstanceOf(MqodEnvelopeInterface::class, $result);
    }
  }

  /**
   * Provider for the getQueue test.
   *
   * @return array
   *   The provided test cases.
   */
  public function getQueueProvider() {
    return [
      [TRUE],
      [FALSE],
    ];
  }

  /**
   * Test whether we read a message.
   *
   * @param bool $read_status
   *   Whether the queue open openration is successful.
   *
   * @covers ::__construct
   * @covers ::__destruct
   * @covers ::getQueue
   * @dataProvider readFromQueueProvider
   */
  public function testReadFromQueue($read_status) {
    $connection = $this->prophesize(MqConnection::class);
    $mqod_envelope = $this->prophesize(MqodEnvelopeInterface::class);
    $mqgmo_envelope = $this->prophesize(MqgmoEnvelopeInterface::class);
    $mqod_envelope->toGetEnvelope(Argument::any(), Argument::any())->willReturn($mqgmo_envelope->reveal());
    $resolver = $this->prophesize(ClassResolverInterface::class);
    $logger_factory = $this->prophesize(LoggerChannelFactoryInterface::class);
    $logger = $this->prophesize(LoggerChannelInterface::class);
    if ($read_status) {
      $connection->get(Argument::any())->shouldBeCalled();
      $mqgmo_envelope->getMessage()->willReturn('message');
    }
    else {
      $connection->get(Argument::any())->willThrow(\Exception::class);
      $this->expectException(\exception::class);
    }
    $logger_factory->get(Argument::any())->willReturn($logger->reveal());
    $manager = new MqManager($resolver->reveal(), $logger_factory->reveal());
    $result = $manager->readFromQueue($connection->reveal(), $mqod_envelope->reveal(), 10);
    if ($read_status) {
      $this->assertNotNull($result);
    }
  }

  /**
   * Provider for the read from queue test.
   *
   * @return array
   *   The provided test cases.
   */
  public function readFromQueueProvider() {
    return [
      [TRUE],
      [FALSE],
    ];
  }

  /**
   * Test for the commit method.
   */
  public function testCommit() {
    $connection = $this->prophesize(MqConnection::class);
    $envelope = $this->prophesize(MqodEnvelopeInterface::class);
    $resolver = $this->prophesize(ClassResolverInterface::class);
    $logger_factory = $this->prophesize(LoggerChannelFactoryInterface::class);
    $logger = $this->prophesize(LoggerChannelInterface::class);
    $resolver->getInstanceFromDefinition(MqEnvelope::class)->willReturn($envelope->reveal());
    $connection->commit(Argument::any())->shouldBeCalled();
    $logger_factory->get(Argument::any())->willReturn($logger->reveal());
    $manager = new MqManager($resolver->reveal(), $logger_factory->reveal());
    $result = $manager->commit($connection->reveal());
    $this->assertInstanceOf(EnvelopeInterface::class, $result);
  }

  /**
   * Test for the rollback method.
   */
  public function testRollback() {
    $connection = $this->prophesize(MqConnection::class);
    $envelope = $this->prophesize(MqodEnvelopeInterface::class);
    $resolver = $this->prophesize(ClassResolverInterface::class);
    $logger_factory = $this->prophesize(LoggerChannelFactoryInterface::class);
    $logger = $this->prophesize(LoggerChannelInterface::class);
    $resolver->getInstanceFromDefinition(MqEnvelope::class)->willReturn($envelope->reveal());
    $connection->rollback(Argument::any())->shouldBeCalled();
    $logger_factory->get(Argument::any())->willReturn($logger->reveal());
    $manager = new MqManager($resolver->reveal(), $logger_factory->reveal());
    $result = $manager->rollback($connection->reveal());
    $this->assertInstanceOf(EnvelopeInterface::class, $result);
  }

  /**
   * Test whether process a queue.
   *
   * @param string $message
   *   A test message.
   * @param bool $class
   *   Whether the resource is OK.
   * @param bool $read_status
   *   Whether reading the message is OK.
   * @param bool $persist
   *   Whether the persist flag is set.
   * @param bool $process_result
   *   The return value of the resource processing.
   * @param bool $commit
   *   Whether the commit flag is set.
   * @param bool $persist_exception
   *   Whether the resource processing threw an exception.
   *
   * @covers ::__construct
   * @covers ::__destruct
   * @covers ::getQueue
   * @dataProvider processQueueProvider
   */
  public function testProcessQueue(string $message, bool $class, bool $read_status, bool $persist, bool $process_result, bool $commit, bool $persist_exception) {
    $connection = $this->prophesize(MqConnection::class);
    $mqod = $this->prophesize(MqodEnvelopeInterface::class);
    $mqgmo = $this->prophesize(MqgmoEnvelopeInterface::class);
    $envelope = $this->prophesize(EnvelopeInterface::class);
    $resolver = $this->prophesize(ClassResolverInterface::class);
    $logger_factory = $this->prophesize(LoggerChannelFactoryInterface::class);
    $logger = $this->prophesize(LoggerChannelInterface::class);
    if ($class) {
      // Resource is OK.
      $resource = $this->prophesize(MqDrushResourceInterface::class);
      $resolver->getInstanceFromDefinition(MqEnvelope::class)->willReturn($envelope->reveal());
      if ($read_status) {
        // Read is OK.
        $mqgmo->getMessage()->willReturn($message);
        $mqod->toGetEnvelope(Argument::any(), Argument::any())->willReturn($mqgmo->reveal());
        $connection->get(Argument::any())->willReturn($message);

        if ($persist) {
          // Persist flag is true.
          if ($persist_exception) {
            $resource->process(Argument::any())->willThrow(\Exception::class);
          }
          else {
            $resource->process(Argument::any())->willReturn($process_result);
          }
          if ($process_result) {
            $logger->info(Argument::any(), Argument::any())->shouldBeCalled();
            $logger->error(Argument::any(), Argument::any())->shouldNotBeCalled();
            if ($commit) {
              $connection->commit(Argument::any())->shouldBeCalled();
            }
            else {
              $connection->commit(Argument::any())->shouldNotBeCalled();
            }
          }
          else {
            if ($persist_exception) {
              $connection->commit(Argument::any())->shouldNotBeCalled();
            }
            else {

            }
            $logger->error(Argument::any(), Argument::any())->shouldBeCalled();
            $connection->rollback(Argument::any())->shouldBeCalled();
            $logger->info(Argument::any(), Argument::any())->shouldBeCalled();
          }
        }
        else {
          // Persist flag is false.
          $resource->process(Argument::any())->shouldNotBeCalled();
          $connection->rollback(Argument::any())->shouldBeCalled();
          $connection->commit(Argument::any())->shouldNotBeCalled();
        }
      }
      else {
        // Read failed.
        $mqgmo->getMessage()->shouldNotBeCalled();
        $mqod->toGetEnvelope(Argument::any(), Argument::any())->willReturn($mqgmo->reveal());
        if (empty($message)) {
          // Last Message.
          $connection->get(Argument::any())->willThrow(\Exception::class);
          $connection->lastCommandSuccess()->willReturn(TRUE);
          $logger->error(Argument::any(), Argument::any())->shouldNotBeCalled();
          $connection->rollback(Argument::any())->shouldNotBeCalled();
        }
        else {
          // Read Error.
          $connection->get(Argument::any())->willThrow(\Exception::class);
          $connection->lastCommandSuccess()->willReturn(FALSE);
          $logger->error(Argument::any(), Argument::any())->shouldBeCalled();
          $connection->rollback(Argument::any())->shouldBeCalled();
        }
        $logger->info(Argument::any(), Argument::any())->shouldNotBeCalled();
        $logger->critical(Argument::any(), Argument::any())->shouldNotBeCalled();
        $connection->commit(Argument::any())->shouldNotBeCalled();

      }

    }
    else {
      // Resource is not OK.
      $resource = $this->prophesize(self::class);
      $resolver->getInstanceFromDefinition(self::class)->willReturn($resource->reveal());
      $logger->critical(Argument::any(), Argument::any())->shouldBeCalled();
      $connection->commit(Argument::any())->shouldNotBeCalled();
    }

    $mqod->getQueueName()->willReturn('queue_name');
    $resolver->getInstanceFromDefinition(StdoutResource::class)->willReturn($resource->reveal());
    $logger_factory->get(Argument::any())->willReturn($logger->reveal());
    $manager = new MqManager($resolver->reveal(), $logger_factory->reveal());
    if ($class) {
      $manager->processQueue($connection->reveal(), $mqod->reveal(), StdoutResource::class, $persist, $commit, 1, 10);
    }
    else {
      $manager->processQueue($connection->reveal(), $mqod->reveal(), self::class, $persist, $commit, 1, 10);
    }
  }

  /**
   * Provider for the process queue test.
   *
   * @return array
   *   The provided test cases.
   */
  public function processQueueProvider() {
    return [
      // All OK,.
      ['A message', TRUE, TRUE, TRUE, TRUE, TRUE, FALSE],
      // Resource is not OK.
      ['A message', FALSE, TRUE, TRUE, TRUE, TRUE, FALSE],
      // Read failed.
      ['A message', TRUE, FALSE, TRUE, TRUE, TRUE, FALSE],
      // No more messages.
      ['', TRUE, FALSE, TRUE, TRUE, TRUE, FALSE],
      // Persist flag is false.
      ['A message', TRUE, TRUE, FALSE, TRUE, TRUE, FALSE],
      // Persist flag is false.
      ['A message', TRUE, TRUE, FALSE, TRUE, TRUE, FALSE],
      // Process method returned false.
      ['A message', TRUE, TRUE, TRUE, FALSE, TRUE, FALSE],
      // Process method threw exception.
      ['A message', TRUE, TRUE, TRUE, FALSE, TRUE, TRUE],
      // Commit flag is not set.
      ['A message', TRUE, TRUE, TRUE, TRUE, FALSE, FALSE],
    ];
  }

}
