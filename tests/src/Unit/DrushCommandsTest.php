<?php

namespace Drupal\Tests\mqclient_drush\Unit;

use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\mqclient_drush\Commands\MqCommands;
use Drupal\mqclient_drush\MqDrushResourceInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\user\UserInterface;
use Drush\Log\DrushLoggerManager;
use gu\mqclient\envelopes\EnvelopeFactory;
use gu\mqclient\envelopes\MqcnoEnvelopeInterface;
use gu\mqclient\envelopes\MqgmoEnvelopeInterface;
use gu\mqclient\envelopes\MqodEnvelopeInterface;
use gu\mqclient\MqConnection;
use gu\mqclient\MqException;
use gu\mqclient\stamps\MqcnoStamp;
use gu\mqclient\stamps\StampFactory;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Drush command tests.
 *
 * @coversDefaultClass \Drupal\mqclient_drush\Commands\MqCommands
 */
class DrushCommandsTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * Test whether we can test connections successfully.
   *
   * @param bool $connection_status
   *   Whether the connection test is successful.
   *
   * @covers ::__construct
   * @covers ::__destruct
   * @covers ::testConnection
   * @covers ::connect
   * @dataProvider getConnectionStatusProvider
   */
  public function testConnectTests(bool $connection_status) {
    $user = $this->prophesize(UserInterface::class);
    $storage = $this->prophesize(EntityStorageInterface::class);
    $storage->load(Argument::any())->willReturn($user->reveal());
    $manager = $this->prophesize(EntityTypeManagerInterface::class);
    $manager->getStorage('user')->willReturn($storage->reveal());
    $account_switcher = $this->prophesize(AccountSwitcherInterface::class);

    $resolver = $this->prophesize(ClassResolverInterface::class);
    $mqcno_stamp = $this->prophesize(MqcnoStamp::class);
    $stamp_factory = $this->prophesize(StampFactory::class);
    $stamp_factory->createConnectionStamp(Argument::any(), Argument::any(), Argument::any())->willReturn($mqcno_stamp->reveal());
    $resolver->getInstanceFromDefinition(StampFactory::class)->willReturn($stamp_factory->reveal());

    $mqcno_envelope = $this->prophesize(MqcnoEnvelopeInterface::class);
    $envelope_factory = $this->prophesize(EnvelopeFactory::class);
    $envelope_factory->createConnectionEnvelope(Argument::any(), Argument::any())->willReturn($mqcno_envelope->reveal());
    $resolver->getInstanceFromDefinition(EnvelopeFactory::class)->willReturn($envelope_factory->reveal());
    $connection = $this->prophesize(MqConnection::class);
    $connection->processCallResult()->willReturn(NULL);
    $connection->isConnected()->willReturn(FALSE);
    if ($connection_status) {
      $connection->connect(Argument::any())->willReturn($mqcno_envelope);
      $connection->lastReason()->willReturn(0);
    }
    else {
      $connection->connect(Argument::any())->willThrow(new MqException(''));
      $connection->lastReason()->willReturn(5);
    }
    $connection->disconnect(Argument::any())->willReturn(NULL);

    $resolver->getInstanceFromDefinition(MqConnection::class)->willReturn($connection->reveal());

    $commands = new MqCommands($resolver->reveal(), $account_switcher->reveal(), $manager->reveal());

    $logger = $this->prophesize(DrushLoggerManager::class);
    $commands->setLogger($logger->reveal());

    $return = $commands->testConnection();
    if ($connection_status) {
      $this->assertEquals(0, $return, "Can detect successful connections");
    }
    else {
      $this->assertNotEquals(0, $return, "Can detect failed connections");
    }
  }

  /**
   * Data provider for testProcessQueue.
   *
   * @return array
   *   Provided data.
   */
  public function getConnectionStatusProvider(): array {
    return [
      [TRUE],
      [FALSE],
    ];
  }

  /**
   * Test processing a queue with an authorized user.
   *
   * @param bool $persist
   *   The persist option.
   * @param bool $commit
   *   The commit option.
   * @param int $count
   *   The count option.
   * @param bool $open_status
   *   Whether we can open a connection.
   * @param bool $read_status
   *   Whether we can read a message.
   * @param string $message
   *   A message to process.
   * @param bool $process_result
   *   The result of processing the message in the resource.
   * @param bool $process_exception
   *   Whether to generate an exception while processing.
   *
   * @covers ::__construct
   * @covers ::__destruct
   * @covers ::processQueue
   * @covers ::connect
   * @dataProvider getProcessQueueProvider
   */
  public function testProcessQueue(bool $persist, bool $commit, int $count, bool $open_status, bool $read_status, string $message, bool $process_result, bool $process_exception) {
    $user = $this->prophesize(UserInterface::class);
    $user->hasPermission(Argument::any())->willReturn(TRUE);
    $storage = $this->prophesize(EntityStorageInterface::class);
    $storage->load(Argument::any())->willReturn($user->reveal());
    $manager = $this->prophesize(EntityTypeManagerInterface::class);
    $manager->getStorage('user')->willReturn($storage->reveal());
    $account_switcher = $this->prophesize(AccountSwitcherInterface::class);

    $resolver = $this->prophesize(ClassResolverInterface::class);

    $mqod_stamp = $this->prophesize(MqcnoStamp::class);
    $stamp_factory = $this->prophesize(StampFactory::class);
    $stamp_factory->createOpenObjectStamp(Argument::any(), Argument::any())->willReturn($mqod_stamp->reveal());
    $resolver->getInstanceFromDefinition(StampFactory::class)->willReturn($stamp_factory->reveal());

    $mqgmo_envelope = $this->prophesize(MqgmoEnvelopeInterface::class);
    $mqgmo_envelope->getMessage()->willReturn($message);
    $mqod_envelope = $this->prophesize(MqodEnvelopeInterface::class);
    $mqgmo = $mqgmo_envelope->reveal();
    $mqod_envelope->toGetEnvelope(Argument::any(), Argument::any())->willReturn($mqgmo);
    $mqod_envelope->toGetEnvelope(Argument::any())->willReturn($mqgmo);
    $mqod = $mqod_envelope->reveal();
    $envelope_factory = $this->prophesize(EnvelopeFactory::class);
    $envelope_factory->createOpenObjectEnvelope(Argument::any(), Argument::any())->willReturn($mqod);
    $resolver->getInstanceFromDefinition(EnvelopeFactory::class)->willReturn($envelope_factory->reveal());

    $connection = $this->prophesize(MqConnection::class);
    $connection->processCallResult()->willReturn(NULL);
    $connection->isConnected()->willReturn(TRUE);

    if ($open_status) {
      $connection->open(Argument::any())->shouldBeCalled();
      if ($read_status) {
        $connection->get(Argument::any())->shouldBeCalled();
      }
      else {
        $connection->get(Argument::any())->willThrow(new MqException(''));
      }
    }
    else {
      $connection->open(Argument::any())->willThrow(new MqException(''));
      $connection->get(Argument::any())->shouldNotBeCalled();
    }
    if ($open_status && $read_status) {
      $connection->lastReason()->willReturn(0);
    }
    else {
      $connection->lastReason()->willReturn(5);
    }

    $connection->lastCommandSuccess(Argument::any())->WillReturn($open_status && $read_status);
    $resolver->getInstanceFromDefinition(MqConnection::class)->willReturn($connection->reveal());

    $logger = $this->prophesize(DrushLoggerManager::class);

    $commands = new MqCommands($resolver->reveal(), $account_switcher->reveal(), $manager->reveal());
    $commands->setLogger($logger->reveal());

    $resource = $this->prophesize(MqDrushResourceInterface::class);
    if ($persist) {
      if ($process_exception) {
        $resource->process($message)->willThrow(new \Exception(''));
      }
      else {
        $resource->process($message)->willReturn($process_result);
      }
      $resolver->getInstanceFromDefinition(MqDrushResourceInterface::class)->willReturn($resource->reveal());
    }
    else {
      $resolver->getInstanceFromDefinition(MqDrushResourceInterface::class)->willReturn($resource->reveal());
      $resource->process(Argument::any())->shouldNotBeCalled();
    }
    if ($commit && $open_status && $read_status && $process_result && !$process_exception) {
      $connection->commit(Argument::any())->shouldBeCalled();
      $connection->rollback(Argument::any())->shouldBeCalled();
    }
    elseif ($open_status) {
      $connection->commit(Argument::any())->shouldNotBeCalled();
      $connection->rollback(Argument::any())->shouldBeCalled();
    }
    else {
      $connection->commit(Argument::any())->shouldNotBeCalled();
      $connection->rollback(Argument::any())->shouldNotBeCalled();
    }
    $connection->disconnect(Argument::any())->shouldBeCalled();
    $return = $commands->processQueue('sample_queue', MqDrushResourceInterface::class, [
      'persist' => $persist,
      'commit' => $commit,
      'count' => $count,
      'user' => 1,
      'max_message_size' => 5000,
    ]);
    if ($open_status && $read_status) {
      $this->assertEquals(0, $return, 'Normal completion');
    }
    else {
      $this->assertNotEquals(0, $return, 'Abnormal completion');
    }
  }

  /**
   * Data provider for testProcessQueue.
   *
   * @return array
   *   Provided data.
   */
  public function getProcessQueueProvider(): array {
    return [
      // Successful calls.
      [TRUE, TRUE, 3, TRUE, TRUE, 'A message', TRUE, FALSE],
      // No messages.
      [TRUE, TRUE, 0, TRUE, TRUE, 'A message', TRUE, FALSE],
      // Failed to read.
      [TRUE, TRUE, 3, TRUE, FALSE, 'A message', TRUE, TRUE],
      // Failed to persist.
      [TRUE, TRUE, 3, TRUE, TRUE, '', TRUE, TRUE],
      // Failed to connect.
      [TRUE, TRUE, 3, FALSE, TRUE, 'A message', TRUE, FALSE],
      // Total failure.
      [TRUE, TRUE, 3, FALSE, FALSE, '', FALSE, TRUE],
      // Commit option.
      [TRUE, FALSE, 3, TRUE, TRUE, 'A message', TRUE, TRUE],
      // Persist option.
      [FALSE, TRUE, 3, TRUE, TRUE, 'A MESSAGE', TRUE, TRUE],
      // High number of messages.
      [TRUE, TRUE, 3000, TRUE, TRUE, 'A message', TRUE, FALSE],
    ];
  }

  /**
   * Test the ProcessQueue method with an unauthorized user.
   *
   * @param string|null $user_id
   *   A user ID or name.
   *
   * @covers ::processQueue
   * @covers ::getAccount
   * @dataProvider getProcessQueueUnahorizedProvider
   */
  public function testProcessQueueUnauthorized($user_id) {
    $user = $this->prophesize(UserInterface::class);
    $user->hasPermission(Argument::any())->willReturn(FALSE);
    $storage = $this->prophesize(EntityStorageInterface::class);
    if (is_null($user_id) || is_numeric($user_id)) {
      $storage->load(Argument::any())->willReturn($user->reveal());
    }
    else {
      $storage->loadByProperties(Argument::any())->willReturn([$user->reveal()]);
    }
    $manager = $this->prophesize(EntityTypeManagerInterface::class);
    $manager->getStorage('user')->willReturn($storage->reveal());
    $account_switcher = $this->prophesize(AccountSwitcherInterface::class);
    $account_switcher->switchTo(Argument::any())->shouldNotBeCalled();

    $resolver = $this->prophesize(ClassResolverInterface::class);
    $resolver->getInstanceFromDefinition(Argument::any())->shouldNotBeCalled();
    $logger = $this->prophesize(DrushLoggerManager::class);
    $logger->error(Argument::any(), Argument::any())->shouldBeCalled();

    $commands = new MqCommands($resolver->reveal(), $account_switcher->reveal(), $manager->reveal());
    $commands->setLogger($logger->reveal());

    $return = $commands->processQueue('sample_queue', MqDrushResourceInterface::class, [
      'persist' => TRUE,
      'commit' => TRUE,
      'count' => 100,
      'user' => $user_id,
    ]);
    $this->assertGreaterThan(0, $return);
  }

  /**
   * Data provider for testProcessQueueUnauthorized.
   */
  public function getProcessQueueUnahorizedProvider() {
    return [
      [NULL],
      ['0'],
      ['10'],
      ['name'],
    ];
  }

}
