<?php

namespace Drupal\mqclient_drush\Commands;

use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\mqclient_drush\MqDrushResourceInterface;
use Drupal\mqclient_drush\Resource\StdoutResource;
use Drush\Commands\DrushCommands;
use gu\mqclient\envelopes\EnvelopeFactory;
use gu\mqclient\envelopes\MqodEnvelopeInterface;
use gu\mqclient\envelopes\MqQueueMessageEnvelopeInterface;
use gu\mqclient\MqConnection;
use gu\mqclient\stamps\StampFactory;

/**
 * Drush commands for gu_kop.
 */
class MqCommands extends DrushCommands {

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected ClassResolverInterface $resolver;

  /**
   * The account switcher service.
   *
   * @var \Drupal\Core\Session\AccountSwitcherInterface
   */
  protected AccountSwitcherInterface $accountSwitcher;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The MQ connection.
   *
   * @var \gu\mqclient\MqConnection|null
   */
  protected ?MqConnection $mq;

  /**
   * Creates an instance of MqCommands.
   *
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $resolver
   *   The class resolver service.
   * @param \Drupal\Core\Session\AccountSwitcherInterface $account_switcher
   *   The account switcher service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(ClassResolverInterface $resolver, AccountSwitcherInterface $account_switcher, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct();

    $this->resolver = $resolver;
    $this->accountSwitcher = $account_switcher;
    $this->entityTypeManager = $entity_type_manager;
    $this->mq = NULL;
  }

  /**
   * Destroys the instance.
   */
  public function __destruct() {
    $this->disconnect();
  }

  /**
   * Process all messages in a queue.
   *
   * @param string $queue_name
   *   The name of a queue.
   * @param string $class_name
   *   The class name of a resource.
   * @param array $options
   *   An array of options.
   *
   * @command mq:process_queue
   *
   * @option max_message_size
   *   The maximum message size to process, as integer, in bytes.
   *   Default: 4194304 (4 MB).
   * @options persist
   *   Persist the incoming message in Drupal.
   *   Default: false.
   * @options commit
   *   Commit read messages to MQ.
   *   Default: false.
   * @option count
   *   How many messages to read.
   *   Default: 10.
   * @option user
   *   A user id. The command will run as this user.
   *   Default: 0.
   *
   * @return int
   *   A return value to be used in error monitoring.
   */
  public function processQueue(
    string $queue_name,
    string $class_name,
    array $options = [
      'max_message_size' => MqQueueMessageEnvelopeInterface::MAX_MESSAGE_SIZE_DEFAULT,
      'persist' => FALSE,
      'commit' => FALSE,
      'count' => 10,
      'user' => 0,
    ]): int {
    $user = $this->getAccount($options['user'] ?? 0);
    if (!$user instanceof AccountInterface) {
      $this->logger()->error('The provided user "{@user}" does not exist or is not authorized', [
        '@user' => $options['user'],
      ]);
      return 1;
    }
    $this->accountSwitcher->switchTo($user);
    try {
      $this->connect();
      $queue = $this->openQueue($queue_name);
      $this->logger()->debug('Queue {@queue} is open', ['@queue' => $queue_name]);
      $counter = 0;
      do {
        // @todo Handle high memory usage if needed.
        // @todo Can we instantiate the resource outside the loop?
        $resource = $this->resolver->getInstanceFromDefinition($class_name);
        $commit = FALSE;
        if ($resource instanceof MqDrushResourceInterface) {
          try {
            // MQ messages are string buffers converted from C pointers.
            // In our use case, messages are always strings, or NULL when
            // no more messages are available.
            try {
              $counter++;
              $message = $this->readFromQueue($queue, $options['max_message_size']);
            }
            catch (\Exception $e) {
              if (!$this->getConnection()->lastCommandSuccess()) {
                // We get a exception when there are no more messages.
                // That is considered a successful command.
                $this->logger()->error("Error reading message from queue {queue}. Error message is: {error}", [
                  'queue' => $queue_name,
                  'error' => $e->getMessage(),
                ]);
              }
              // Rollback to inform MQ that we had an error.
              $this->rollback();
              // Break the loop to avoid more errors.
              break;
            }
            if (!is_null($message)) {
              if ($options['persist']) {
                $commit = $resource->process((string) $message);
              }
              if (!$options['commit']) {
                $commit = FALSE;
              }
              if ($commit) {
                $this->logger()->info('Resource {@resource} has persisted a message from queue {@queue} Successfully', [
                  '@resource' => $class_name,
                  '@queue' => $queue_name,
                ]);
                $this->commit();
                $this->logger()->info('A message was committed to queue {@queue}.', ['@queue' => $queue_name]);
              }
              else {
                if ($options['persist']) {
                  $this->logger()->error('Resource {@resource} failed to persist a message from queue {@queue}', [
                    '@resource' => $class_name,
                    '@queue' => $queue_name,
                  ]);
                }
                $this->rollback();
                $this->logger()->info('A message was rolled back to queue {@queue}.', ['@queue' => $queue_name]);
                break;
              }
            }
          }
          catch (\Exception $e) {
            $this->logger()->error('An error was caught while processing a message from {@queue}', [
              '@queue' => $queue_name,
            ]);
            $this->rollback();
            $this->logger()->info('A message was rolled back to queue {@queue}.', ['@queue' => $queue_name]);
            break;
          }
        }
        else {
          $this->logger()->critical('Resource {@resource} is not a valid instance of interface {@interface}', [
            '@resource' => $class_name,
            '@interface' => MqDrushResourceInterface::class,
          ]);
          break;
        }
      } while (!is_null($message) && $counter < $options['count']);
      // Unless we rollback, MQ will automatically commit any uncommitted
      // messages in the current unit of work when disconnecting.
      $this->rollback();
      $this->getConnection()->disconnect();
      $this->logger()->info('The queue {@queue} was closed.', ['@queue' => $queue_name]);
    }
    catch (\exception $e) {
      $this->logger()->critical('Failed to open queue {@queue} for reason {@reason}', [
        '@queue' => $queue_name,
        '@reason' => $e->getMessage(),
      ]);
    }

    return $this->mq->lastReason();
  }

  /**
   * Test the connection to the MQ Server.
   *
   * @command mq:test_connection
   */
  public function testConnection(): int {
    if ($this->getConnection() !== NULL) {
      $this->disconnect();
    }

    return $this->connect();
  }

  /**
   * Test the connection to the MQ Server.
   *
   * @param string $queue_name
   *   The name of a queue.
   *
   * @command mq:test_queue
   */
  public function testQueue(string $queue_name): void {
    $this->processQueue($queue_name, StdoutResource::class, [
      'persist' => TRUE,
      'commit' => FALSE,
      'user' => 1,
      'max_message_size' => MqQueueMessageEnvelopeInterface::MAX_MESSAGE_SIZE_DEFAULT,
    ]);
  }

  /**
   * Remove messages from queue.
   *
   * @param string $queue_name
   *   The name of a queue.
   * @param array $options
   *   An array of options.
   *
   * @command mq:remove_message
   *
   * @option user
   *   A user id. The command will run as this user.
   *   Default: 0.
   *
   * @return int
   *   A return value to be used in error monitoring.
   */
  public function removeMessageFromQueue(
    string $queue_name,
    array $options = [
      'user' => 0,
    ]): int {
    $user = $this->getAccount($options['user'] ?? 0);
    if (!$user instanceof AccountInterface) {
      $this->logger()->error('The provided user "{@user}" does not exist or is not authorized', [
        '@user' => $options['user'],
      ]);
      return 1;
    }

    $this->accountSwitcher->switchTo($user);

    try {
      $this->connect();
      $queue = $this->openQueue($queue_name);
      $this->logger()->debug('Queue {@queue} is open', ['@queue' => $queue_name]);
    }
    catch (\exception $e) {
      $this->logger()->critical('Failed to connect and open queue {@queue} for reason {@reason}', [
        '@queue' => $queue_name,
        '@reason' => $e->getMessage(),
      ]);
      return 2;
    }

    $resource = $this->resolver->getInstanceFromDefinition(StdoutResource::class);
    $message = '';

    try {
      $message = $this->readFromQueue($queue);
    }
    catch (\Exception $e) {
      if (!$this->getConnection()->lastCommandSuccess()) {
        // We get a exception when there are no more messages.
        // That is considered a successful command.
        $this->logger()->error("Error reading message from queue {queue}. Error message is: {error}", [
          'queue' => $queue_name,
          'error' => $e->getMessage(),
        ]);
      }
    }

    if ($this->getConnection()->lastCommandSuccess()) {
      // We have an actual message.
      try {
        $resource->process((string) $message);
        $answer = $this->io()->ask('Commit this message to the queue?', 'y');
        if (in_array($answer, ['y', 'Y', 'yes', 'Yes', 'YES'])) {
          $this->commit();
          $this->logger()->info('A message was committed to queue {@queue}.', ['@queue' => $queue_name]);
        }
      }
      catch (\Exception $e) {
        $this->logger()->error('An error was caught while processing a message from {@queue}', [
          '@queue' => $queue_name,
        ]);
      }
    }
    // Unless we rollback, MQ will automatically commit any uncommitted
    // messages in the current unit of work when disconnecting.
    $this->rollback();
    $this->getConnection()->disconnect();
    $this->logger()->info('The queue {@queue} was closed.', ['@queue' => $queue_name]);

    return $this->mq->lastReason();
  }

  /**
   * Connect to the server, if not already connected.
   *
   * @return int
   *   The reason code of the last MQ call.
   */
  protected function connect(): int {
    if (!$this->mq instanceof MqConnection) {
      $this->mq = $this->resolver->getInstanceFromDefinition(MqConnection::class);
    }
    if ($this->getConnection()->isConnected()) {
      return defined("MQSERIES_MQRC_NONE") ? MQSERIES_MQRC_NONE : 0;
    }
    try {
      $stamp_factory = $this->resolver->getInstanceFromDefinition(StampFactory::class);
      /** @var \gu\mqclient\stamps\MqcnoStampInterface $mqcno_stamp */
      $mqcno_stamp = $stamp_factory->createConnectionStamp(getenv('MQ_SERVER_ADDRESS'), getenv('MQ_SERVER_PORT'), getenv('MQ_SERVER_CHANNEL'));
      $mqcno_stamp->enableSsl(getenv('MQ_SERVER_KEY_REPOSITORY'));
      $mqcno_stamp->setAuth(trim(getenv('MQ_SERVER_AUTH_USER')), trim(getenv('MQ_SERVER_AUTH_PASS')));
      $envelope_factory = $this->resolver->getInstanceFromDefinition(EnvelopeFactory::class);
      $mqcno_envelope = $envelope_factory->createConnectionEnvelope($mqcno_stamp, getenv('MQ_SERVER_QUEUE_MANAGER'));
      $this->mq->connect($mqcno_envelope);
      $this->logger()->debug('Connected to queue manager {@qmanager}', ['qmanager' => getenv('MQ_SERVER_QUEUE_MANAGER')]);
    }
    catch (\Exception $e) {
      $this->logger()->critical('Failed to connect to queue manager {qmanager} for reason: {reason}', [
        'qmanager' => getenv('MQ_SERVER_QUEUE_MANAGER'),
        'reason' => $e->getMessage(),
      ]);
    }
    return $this->getConnection()->lastReason();
  }

  /**
   * Disconnect from the server, if connected.
   */
  protected function disconnect() {
    if ($this->mq instanceof MqConnection) {
      $this->mq->disconnect();
      $this->logger()->debug('Disconnected from queue manager {@qmanager}', ['qmanager' => getenv('MQ_SERVER_QUEUE_MANAGER')]);
    }
  }

  /**
   * Open a queue on the existing MQ connection.
   *
   * @param string $queue
   *   The queue name.
   * @param bool $readonly
   *   Open the queue for MQGET call only.
   *
   * @return \gu\mqclient\envelopes\MqodEnvelopeInterface
   *   An Mqod Envelope.
   */
  protected function openQueue(string $queue, bool $readonly = TRUE): MqodEnvelopeInterface {
    $envelope_factory = $this->resolver->getInstanceFromDefinition(EnvelopeFactory::class);
    $stamp_factory = $this->resolver->getInstanceFromDefinition(StampFactory::class);
    /** @var \gu\mqclient\envelopes\MqodEnvelopeInterface */
    $mqod = $envelope_factory->createOpenObjectEnvelope($stamp_factory->createOpenObjectStamp($queue), $readonly);
    $this->getConnection()->open($mqod);
    return $mqod;
  }

  /**
   * Read all messages from a queue.
   *
   * This function stops reading when an error occurs.
   *
   * @param \gu\mqclient\envelopes\MqodEnvelopeInterface $mqod
   *   An MQOD interface.
   * @param int $max_message_size
   *   The maximum message size to get from the queue.
   *
   * @return mixed
   *   Can return a string or something else stored in a string buffer.
   *   Returns NULL if there are no messages in the queue.
   */
  protected function readFromQueue(MqodEnvelopeInterface $mqod, int $max_message_size = MqQueueMessageEnvelopeInterface::MAX_MESSAGE_SIZE_DEFAULT) {
    $envelope = $mqod->toGetEnvelope(TRUE, $max_message_size);
    $this->getConnection()->get($envelope);
    return $envelope->getMessage();
  }

  /**
   * Commit the last unit of work.
   */
  protected function commit() {
    $this->getConnection()->commit();
  }

  /**
   * Roll back the last unit of work.
   */
  protected function rollback() {
    $this->getConnection()->rollback();
  }

  /**
   * Get the current connection object.
   *
   * @return \gu\mqclient\MqConnection
   *   The current connection object, or null.
   */
  protected function getConnection() {
    return $this->mq;
  }

  /**
   * Sets the current connection.
   *
   * @param \gu\mqclient\MqConnection $connection
   *   The connection.
   */
  protected function setConnection(MqConnection $connection): void {
    $this->mq = $connection;
  }

  /**
   * Get the user account from the connect options.
   *
   * @param string $user_id_or_name
   *   A user ID or name.
   *
   * @return \Drupal\Core\Session\AccountInterface|null
   *   The user account or null if the user is not found, or not authorized.
   */
  protected function getAccount(string $user_id_or_name) {
    $user = NULL;
    if (is_numeric($user_id_or_name)) {
      $uid = (int) $user_id_or_name;
      $user = $this->entityTypeManager->getStorage('user')->load($uid);
    }
    else {
      $users = $this->entityTypeManager
        ->getStorage('user')
        ->loadByProperties([
          'name' => $user_id_or_name,
        ]);
      if (!empty($users)) {
        $user = reset($users);
      }
    }
    if ($user instanceof AccountInterface && $user->hasPermission('connect to mq')) {
      return $user;
    }
    return NULL;
  }

}
