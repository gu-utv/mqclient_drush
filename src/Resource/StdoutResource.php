<?php

namespace Drupal\mqclient_drush\Resource;

use Drupal\mqclient_drush\MqDrushResourceInterface;

/**
 * A dummy resource that outputs some messages to stdout.
 */
class StdoutResource implements MqDrushResourceInterface {

  /**
   * {@inheritdoc}
   */
  public function process(string $message): bool {
    // Do not show short messages to avoid risky tests.
    if (strlen($message) > 10) {
      echo "\n======BEGIN MESSAGE======\n";
      echo $message;
      echo "\n======END MESSAGE======\n";
    }
    return self::RETURN_FOR_SAVE_FAILURE;
  }

}
