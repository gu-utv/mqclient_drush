<?php

namespace Drupal\mqclient_drush;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drush\Commands\DrushCommands;
use gu\mqclient\envelopes\MqodEnvelopeInterface;
use gu\mqclient\envelopes\MqQueueMessageEnvelopeInterface;
use gu\mqclient\MqConnection;

/**
 * Base class for Drush commands.
 */
abstract class MqCommandsBase extends DrushCommands {

  /**
   * The MQ Manager.
   *
   * @var \Drupal\mqclient_drush\MqManager
   */
  protected MqManager $manager;

  /**
   * The account switcher service.
   *
   * @var \Drupal\Core\Session\AccountSwitcherInterface
   */
  protected AccountSwitcherInterface $accountSwitcher;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The MQ connection.
   *
   * @var \gu\mqclient\MqConnection|null
   */
  protected ?MqConnection $mq = NULL;

  /**
   * The connection settings.
   *
   * @var array
   */
  protected array $connectionSettings = [
    'host' => '',
    'port' => '',
    'channel' => '',
    'queue_manager' => '',
    'key_repository' => '',
    'username' => '',
    'password' => '',

  ];

  /**
   * Creates an instance of MqCommands.
   *
   * @param \Drupal\mqclient_drush\MqManager $manager
   *   The queue manager service.
   * @param \Drupal\Core\Session\AccountSwitcherInterface $account_switcher
   *   The account switcher service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(MqManager $manager, AccountSwitcherInterface $account_switcher, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct();

    $this->manager = $manager;
    $this->accountSwitcher = $account_switcher;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Destroys the instance.
   */
  public function __destruct() {
    $this->disconnect();
  }

  /**
   * Implement this to set ConnectionSettings.
   *
   * @return mixed
   *   if you need to return when implementing this.
   */
  abstract public function init(...$arguments);

  /**
   * Connect to the server specified in ConnectSettings.
   */
  protected function connect() {
    if ($this->getConnection() instanceof MqConnection) {
      $this->disconnect();
    }
    $connection = $this->manager->getConnection(
      $this->connectionSettings['host'],
      $this->connectionSettings['port'],
      $this->connectionSettings['channel'],
      $this->connectionSettings['queue_manager'],
      $this->connectionSettings['key_repository'],
      $this->connectionSettings['username'],
      $this->connectionSettings['password']
    );
    $this->setConnection($connection);
  }

  /**
   * Test the connection to the MQ Server.
   */
  protected function connectionTest() {
    if (!empty($this->getConnection())) {
      $this->disconnect();
    }
    $this->connect();
  }

  /**
   * Test reading a message from a queue.
   */
  protected function queueTest($queue_name): ?string {
    if (empty($this->getConnection())) {
      $this->connect();
    }
    $queue = $this->openQueue($queue_name);
    if ($queue instanceof MqodEnvelopeInterface) {
      $message = $this->readFromQueue($queue);
      $this->rollback();
      return $message;
    }
    return NULL;
  }

  /**
   * Disconnect from the server, if connected.
   */
  protected function disconnect() {
    if ($this->getConnection() instanceof MqConnection) {
      $this->getConnection()->disconnect();
      $this->logger()->debug('Disconnected from queue manager {@qmanager}', ['qmanager' => getenv('MQ_SERVER_QUEUE_MANAGER')]);
    }
  }

  /**
   * Open a queue on the existing MQ connection.
   *
   * @param string $queue
   *   The queue name.
   * @param bool $readonly
   *   Open the queue for MQGET call only.
   *
   * @return \gu\mqclient\envelopes\MqodEnvelopeInterface
   *   An Mqod Envelope.
   */
  protected function openQueue(string $queue, bool $readonly = TRUE): MqodEnvelopeInterface {
    return $this->manager->getQueue($this->getConnection(), $queue, $readonly);
  }

  /**
   * Read all messages from a queue.
   *
   * This function stops reading when an error occurs.
   *
   * @param \gu\mqclient\envelopes\MqodEnvelopeInterface $mqod
   *   An MQOD interface.
   * @param int $max_message_size
   *   The maximum message size to get from the queue.
   *
   * @return mixed
   *   Can return a string or something else stored in a string buffer.
   *   Returns NULL if there are no messages in the queue.
   */
  protected function readFromQueue(MqodEnvelopeInterface $mqod, int $max_message_size = MqQueueMessageEnvelopeInterface::MAX_MESSAGE_SIZE_DEFAULT) {
    return $this->manager->readFromQueue($this->getConnection(), $mqod, $max_message_size);
  }

  /**
   * Commit the last unit of work.
   */
  protected function commit() {
    $this->manager->commit($this->getConnection());
  }

  /**
   * Roll back the last unit of work.
   */
  protected function rollback() {
    $this->manager->rollback($this->getConnection());
  }

  /**
   * Get the current connection object.
   *
   * @return \gu\mqclient\MqConnection
   *   The current connection object, or null.
   */
  protected function getConnection() {
    return $this->mq;
  }

  /**
   * Sets the current connection.
   *
   * @param \gu\mqclient\MqConnection $connection
   *   The connection.
   */
  protected function setConnection(MqConnection $connection): void {
    $this->mq = $connection;
  }

  /**
   * Process a queue.
   *
   * @param string $user
   *   The user id or name of the user running th eprocess.
   * @param string $queue_name
   *   The MQ Server queue name (not the queue entity).
   * @param string $class_name
   *   The resource that should process MQ messages.
   * @param bool $persist
   *   Whether to persist messages.
   * @param bool $commit
   *   whether to commit messages back to MQ.
   * @param int $count
   *   How many messages to process.
   * @param int $max_message_size
   *   The maximum message size to expect.
   *
   * @return int
   *   Mqseries status code of operation.
   */
  protected function processQueue(string $user, string $queue_name, string $class_name, bool $persist, bool $commit, int $count, int $max_message_size = MqQueueMessageEnvelopeInterface::MAX_MESSAGE_SIZE_DEFAULT): int {
    $account = $this->getAccount($user);
    if (!$account instanceof AccountInterface) {
      $this->logger()->error('The provided user "{@user}" does not exist or is not authorized', [
        '@user' => $user,
      ]);
      return 1;
    }

    $this->accountSwitcher->switchTo($account);
    try {
      $mqod = $this->openQueue($queue_name, TRUE);
    }
    catch (\Exception $e) {
      $this->logger()->critical('Failed to open queue {@queue} for reason {@reason}', [
        '@queue' => $queue_name,
        '@reason' => $e->getMessage(),
      ]);
      return $this->mq->lastReason();
    }
    if (!$mqod instanceof MqodEnvelopeInterface) {
      $this->logger()->critical('Failed to open queue {@queue}', [
        '@queue' => $queue_name,
      ]);
      return $this->mq->lastReason();
    }
    $this->manager->processQueue($this->getConnection(), $mqod, $class_name, $persist, $commit, $count, $max_message_size);
    $this->rollback();

    return MQSERIES_MQCC_OK;
  }

  /**
   * Get the user account from the connect options.
   *
   * @param string $user_id_or_name
   *   A user ID or name.
   *
   * @return \Drupal\Core\Session\AccountInterface|null
   *   The user account or null if the user is not found, or not authorized.
   */
  protected function getAccount(string $user_id_or_name): ?AccountInterface {
    $user = NULL;
    if (is_numeric($user_id_or_name)) {
      $uid = (int) $user_id_or_name;
      $user = $this->entityTypeManager->getStorage('user')->load($uid);
    }
    else {
      $users = $this->entityTypeManager
        ->getStorage('user')
        ->loadByProperties([
          'name' => $user_id_or_name,
        ]);
      if (!empty($users)) {
        $user = reset($users);
      }
    }
    if ($user instanceof AccountInterface && $user->hasPermission('connect to mq')) {
      return $user;
    }
    return NULL;
  }

}
