<?php

namespace Drupal\mqclient_drush;

use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use gu\mqclient\envelopes\EnvelopeFactory;
use gu\mqclient\envelopes\EnvelopeInterface;
use gu\mqclient\envelopes\MqEnvelope;
use gu\mqclient\envelopes\MqodEnvelopeInterface;
use gu\mqclient\envelopes\MqQueueMessageEnvelopeInterface;
use gu\mqclient\MqConnection;
use gu\mqclient\stamps\StampFactory;

/**
 * The Mq Manager service.
 */
class MqManager {

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $resolver;

  /**
   * The logger channel for mqclient_drush.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * MqManager constructor.
   *
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $resolver
   *   The class resolver service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   */
  public function __construct(ClassResolverInterface $resolver, LoggerChannelFactoryInterface $logger_factory) {
    $this->resolver = $resolver;
    $this->logger = $logger_factory->get('mqclient_drush');
  }

  /**
   * Connect to the MQ server and return a connection object.
   *
   * @param string $host
   *   The MQ server host.
   * @param string $port
   *   The MQ server port.
   * @param string $channel
   *   The MQ server channel.
   * @param string $queue_manager
   *   The MQ server queue manager.
   * @param string $key_repository
   *   The path of the client's key repository.
   * @param string $username
   *   The user name to use in the connection.
   * @param string $password
   *   The password of the user.
   *
   * @return \gu\mqclient\MqConnection
   *   An MQ Connection object.
   *
   * @throws \gu\mqclient\MqException
   */
  public function getConnection(string $host, string $port, string $channel, string $queue_manager, string $key_repository = '', string $username = '', string $password = ''): MqConnection {
    /** @var \gu\mqclient\MqConnection $connection */
    $connection = $this->resolver->getInstanceFromDefinition(MqConnection::class);
    /** @var \gu\mqclient\stamps\StampFactoryInterface $stamp_factory */
    $stamp_factory = $this->resolver->getInstanceFromDefinition(StampFactory::class);
    /** @var \gu\mqclient\stamps\MqcnoStampInterface $mqcno_stamp */
    $mqcno_stamp = $stamp_factory->createConnectionStamp($host, $port, $channel);

    if (!empty($key_repository)) {
      $mqcno_stamp->enableSsl($key_repository);
    }
    if (!empty($username)) {
      $mqcno_stamp->setAuth(trim($username), trim($password));
    }
    $envelope_factory = $this->resolver->getInstanceFromDefinition(EnvelopeFactory::class);
    $mqcno_envelope = $envelope_factory->createConnectionEnvelope($mqcno_stamp, $queue_manager);
    $connection->connect($mqcno_envelope);

    return $connection;

  }

  /**
   * Opens an object (mostly a queue) in the MQ Server.
   *
   * @param \gu\mqclient\MqConnection $connection
   *   A connection to the server.
   * @param string $queue
   *   An object name (a queue name).
   * @param bool $readonly
   *   Whether to allow putting messages in the MQ object.
   *
   * @return \gu\mqclient\envelopes\MqodEnvelopeInterface
   *   An MQOD envelop.
   */
  public function getQueue(MqConnection $connection, string $queue, bool $readonly = TRUE): MqodEnvelopeInterface {
    $envelope_factory = $this->resolver->getInstanceFromDefinition(EnvelopeFactory::class);
    $stamp_factory = $this->resolver->getInstanceFromDefinition(StampFactory::class);
    /** @var \gu\mqclient\envelopes\MqodEnvelopeInterface */
    $mqod = $envelope_factory->createOpenObjectEnvelope($stamp_factory->createOpenObjectStamp($queue), $readonly);
    $connection->open($mqod);
    return $mqod;
  }

  /**
   * Reads a message from a queue.
   *
   * @param \gu\mqclient\MqConnection $connection
   *   A connection to the MQ server.
   * @param \gu\mqclient\envelopes\MqodEnvelopeInterface $mqod
   *   An Open object envelop.
   * @param int $max_message_size
   *   The maximum expected message size in the queue.
   *
   * @return mixed
   *   The message returned from reading the queue. can be a string or
   *   something else.
   */
  public function readFromQueue(MqConnection $connection, MqodEnvelopeInterface $mqod, int $max_message_size = MqQueueMessageEnvelopeInterface::MAX_MESSAGE_SIZE_DEFAULT) {
    $envelope = $mqod->toGetEnvelope(TRUE, $max_message_size);
    $connection->get($envelope);
    return $envelope->getMessage();
  }

  /**
   * Commit the current unit of work to the MQ server.
   *
   * @param \gu\mqclient\MqConnection $connection
   *   The connection to the server.
   *
   * @return \gu\mqclient\envelopes\EnvelopeInterface
   *   An envelop containing the command results.
   */
  public function commit(MqConnection $connection): EnvelopeInterface {
    $envelope = $this->resolver->getInstanceFromDefinition(MqEnvelope::class);
    $connection->commit($envelope);
    return $envelope;
  }

  /**
   * Rollback the current unit of work in the MQ server.
   *
   * @param \gu\mqclient\MqConnection $connection
   *   The connection to the server.
   *
   * @return \gu\mqclient\envelopes\EnvelopeInterface
   *   An envelop containing the command results.
   */
  public function rollback(MqConnection $connection): EnvelopeInterface {
    $envelope = $this->resolver->getInstanceFromDefinition(MqEnvelope::class);
    $connection->rollback($envelope);
    return $envelope;
  }

  /**
   * Process a number of messages in an open queue.
   *
   * @param \gu\mqclient\MqConnection $connection
   *   The connection to the MQ server.
   * @param \gu\mqclient\envelopes\MqodEnvelopeInterface $queue
   *   An open object envelop.
   * @param string $class_name
   *   The class that should process every message in the queue.
   * @param bool $persist
   *   Whether to persist the message.
   * @param bool $commit
   *   Whether to commit the message back to MQ.
   * @param int $count
   *   The number of message to process.
   * @param int $max_message_size
   *   The expected max message size for the queue.
   */
  public function processQueue(MqConnection $connection, MqodEnvelopeInterface $queue, string $class_name, bool $persist = FALSE, $commit = FALSE, $count = 10, $max_message_size = MqQueueMessageEnvelopeInterface::MAX_MESSAGE_SIZE_DEFAULT) {
    $queue_name = $queue->getQueueName();
    $counter = 0;
    do {
      // @todo Handle high memory usage if needed.
      // @todo Can we instantiate the resource outside the loop?
      $resource = $this->resolver->getInstanceFromDefinition($class_name);
      if ($resource instanceof MqDrushResourceInterface) {
        try {
          $counter++;
          $message = $this->readFromQueue($connection, $queue, $max_message_size);
        }
        catch (\Exception $e) {
          if (!$connection->lastCommandSuccess()) {
            // We get a exception when there are no more messages.
            // That is considered a successful command.
            $this->logger->error("Error reading message from queue {queue}. Error message is: {error}", [
              'queue' => $queue_name,
              'error' => $e->getMessage(),
            ]);
            // Rollback to inform MQ that we had an error.
            $this->rollback($connection);
            // Break the loop to avoid more errors.
            return 1;
          }
          else {
            // No more messages in the queue.
            return 0;
          }
        }
        if (!is_null($message)) {
          if ($persist) {
            try {
              $process_result = $resource->process((string) $message);
            }
            catch (\Exception $e) {
              $this->logger->error('An error was caught while processing a message from {@queue}', [
                '@queue' => $queue_name,
              ]);
              $this->rollback($connection);
              $this->logger->info('A message was rolled back to queue {@queue}.', ['@queue' => $queue_name]);
              return 1;
            }
            if ($process_result) {
              $this->logger->info('Resource {@resource} has persisted a message from queue {@queue} Successfully', [
                '@resource' => $class_name,
                '@queue' => $queue_name,
              ]);
              if ($commit) {
                $this->commit($connection);
                $this->logger->info('A message was committed to queue {@queue}.', ['@queue' => $queue_name]);
              }
              else {
                // We do nothing to enable reading multiple messages without
                // committing. This is useful when testing the queue.
              }
            }
            else {
              $this->logger->error('Resource {@resource} failed to persist a message from queue {@queue}', [
                '@resource' => $class_name,
                '@queue' => $queue_name,
              ]);
              $this->rollback($connection);
              $this->logger->info('A message was rolled back to queue {@queue}.', ['@queue' => $queue_name]);
              return 1;
            }
          }
          else {
            // This can be useful when debugging.
            $this->rollback($connection);
            return 1;
          }
        }
        else {
          // Empty message, Nothing to do, just commit if requested.
          if ($commit) {
            $this->commit($connection);
            $this->logger->info('An empty message was committed to queue {@queue}.', ['@queue' => $queue_name]);
          }
        }
      }
      else {
        $this->logger->critical('Resource {@resource} is not a valid instance of interface {@interface}', [
          '@resource' => $class_name,
          '@interface' => MqDrushResourceInterface::class,
        ]);
        return 1;
      }
    } while (!is_null($message) && $counter < $count);
    // We reach here when there are still messages in the queue.
    return 0;
  }

}
