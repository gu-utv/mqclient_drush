<?php

namespace Drupal\mqclient_drush;

/**
 * Interface for Drupal resources used by MQ drush wrapper.
 */
interface MqDrushResourceInterface {
  /**
   * Policy constant: Return value for invalid messages.
   */
  public const RETURN_FOR_INVALID_MESSAGE = TRUE;

  /**
   * Policy constant: Return value when saving a message fails.
   */
  public const RETURN_FOR_SAVE_FAILURE = FALSE;

  /**
   * Policy constant: Return value when validation fails.
   */
  public const RETURN_FOR_VALIDATION_ERROR = TRUE;

  /**
   * Policy constant: Return value for successful processing.
   */
  public const RETURN_FOR_SUCCESS = TRUE;

  /**
   * Process a message.
   *
   * This should return one of the constants defined in the interface.
   *
   * @param string $message
   *   The incoming message.
   *
   * @return bool
   *   True means commit to MQ, false means rollback.
   */
  public function process(string $message) : bool;

}
